//
//  MainViewController.swift
//  WeiBo
//
//  Created by lsg604 on 2019/3/15.
//  Copyright © 2019年 lsg604. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addChildViewController(childVc: HomeViewController(), title: "首页1", imageName: "tabbar_home")
        addChildViewController(childVc: MessageViewController(), title: "点击2", imageName: "tabbar_home")
        addChildViewController(childVc: DiscoverViewController(), title: "编3", imageName: "tabbar_home")
        addChildViewController(childVc: HomeViewController(), title: "开始", imageName: "tabbar_home")

    }
    
    // private ：在当前文件中可以访问，但是其他文件不能访问
    private func addChildViewController(childVc : UIViewController, title : String, imageName : String) {
        childVc.title = "首页"
        childVc.tabBarItem.image = UIImage(named: "tabbar_home")
        childVc.tabBarItem.selectedImage = UIImage(named: imageName + "_highlighted")
        let childNav = UINavigationController(rootViewController: childVc)
        addChild(childNav)

    }

}
